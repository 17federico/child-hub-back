# Introducción
Con el objetivo de desarrollar un backend con microservicios para Child-Hub, en este archivo estaremos listando cada uno de ellos, indicando sus entradas y salidas.

Para que esto funcione, es necesario tener creada una base en MySQL bajo el nombre ChildHub.

Para iniciar este proyecto, se deben correr los comandos:

1) Abrir MySQL WorkBrench. Ejecutar el script "dbChildHub.sql". Este dump posee la estructura de tablas y datos de ejemplo. Tener en cuenta que en el archivo "config.json" de la carpeta config, se encuentran los datos de conexión a la base. En caso de poseer un username/password distinto al indicado en dicho archivo, se deberá modificar y volver a compilar (**npm start**).
2) **npm install**
3) **npm start**

# Usuarios
## Crear Usuario

* **URL**

  http://localhost:8000/usuario/crear

* **Metodo:**

  `POST`
  
*  **URL Parametros**

   None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "dni": "[String]",
        "mail": "[String]",
        "nombre": "[String]",
        "apellido": "[String]",
        "password": "[String]",
        "telefono":"[String]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    { 
         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzY3MjQ5NTMsImV4cCI6MTYzNjgxMTM1M30.a2IWLSp7pcsnT1hse0EwPD3seZojWQx_ON3gM7GbOPE",
    "usuario": {
        "snbloqueado": true,
        "id": 12,
        "dni": "32000000",
        "mail": "frca@inworx.com",
        "nombre": "Lol",
        "apellido": "2o0",
        "password": "$2a$08$MGFuC4iSaAsU9MiVeJVwfe7xPm6PG0RCcM9Bi2QgwgFs5YMjBoMZe",
        "telefono": "213213213",
        "updatedAt": "2021-11-12T13:49:13.574Z",
        "createdAt": "2021-11-12T13:49:13.574Z"
      }
    }
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 202 <br />
  * **Contenido:** `Email ya existente`

    * **Codigo:** 202 <br />
  * **Contenido:** `DNI ya existente`

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot POST`

## Listar Usuarios

* **URL**

  http://localhost:8000/usuario/listar

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
   None

* **Data Params**

  None

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    [
        {
        "id": 3,
        "dni": "37762997",
        "nombre": "Mart",
        "apellido": "Vaz",
        "mail": "mrv@ma.com",
        "telefono": "1122",
        "password": "$2a$08$sN5mkflAjH85MwlrciFFy.BEBhouz3RfczZaz7fxjphHi8igVKGQC",
        "snbloqueado": true,
        "createdAt": "2021-11-03T18:55:07.000Z",
        "updatedAt": "2021-11-03T19:11:38.000Z"
        }
    ]
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Buscar Usuarios por DNI

* **URL**

  http://localhost:8000/usuario/buscar

* **Metodo:**

  `GET`
  
*  **URL Parametros**

  None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "dni": "[String]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
        "id": 5,
        "dni": "4354",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "hola@lalo.com",
        "telefono": "213213213",
        "password": "$2a$08$gu9Y72I/4eGdoLrDLi.a5eKW2eRPk8Ht57uGfEqylhz9Ai8kgQfoi",
        "snbloqueado": true,
        "createdAt": "2021-11-03T19:35:58.000Z",
        "updatedAt": "2021-11-03T20:32:57.000Z"
    }
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Buscar Usuario por ID

* **URL**

  http://localhost:8000/usuario/buscar/id

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**

     ```json
    {
        "id": "[Integer]"
    }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
        "id": 5,
        "dni": "4354",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "hola@lalo.com",
        "telefono": "213213213",
        "password": "$2a$08$gu9Y72I/4eGdoLrDLi.a5eKW2eRPk8Ht57uGfEqylhz9Ai8kgQfoi",
        "snbloqueado": true,
        "createdAt": "2021-11-03T19:35:58.000Z",
        "updatedAt": "2021-11-03T20:32:57.000Z"
    }
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Loguear Usuario

* **URL**

  http://localhost:8000/usuario/loguear

* **Metodo:**

  `POST`
  
*  **URL Parametros**

   None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "email": "[String]",
        "password": "[String]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
    ```json
    {
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzY3MjU2NTAsImV4cCI6MTYzNjgxMjA1MH0.QBjtzDDicgUlrpDTm2eT4mrF0LQEKnCCAoSV13ERQIs",
    "usuario": {
        "id": 13,
        "dni": "32000000",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "frcac@inworx.com",
        "telefono": "213213213",
        "password": "$2a$08$Yq8WwQ6DnpQO6m9ZjtHenOIPIH.e5.584DCPsX3P.9ZgQUqxlRBda",
        "snbloqueado": true,
        "createdAt": "2021-11-12T14:00:24.000Z",
        "updatedAt": "2021-11-12T14:00:24.000Z"
      }
    }
    

* **Respuesta No Exitosa:**
* **Codigo:** 202 <br />
    **Contenido:** `Invalid Username`

* **Codigo:** 203 <br />
    **Contenido:** `Invalid username or password`

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`



## Actualizar Usuario

* **URL**

  http://localhost:8000/usuario/actualizar

* **Metodo:**

  `PUT`
  
*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[String]", //Id del usuario a actualizar
        "dni": "[String]", //Nuevo DNI
        "nombre": "[String]", //Nuevo nombre
        "apellido": "[String]", //Nuevo apellido
        "telefono": "[String]", //Nuevo telefono
        "mail": "[String]" //Nuevo mail

    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
    ```json
    {
    "status": 200,
    "data": {
        "id": 10,
        "dni": "30000000",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "frcac@inworx.com",
        "telefono": "213213213",
        "password": "$2a$08$uQSNUIOhpnAy7xRtKuT4XuMlmcci3qykz2hZZzvl6aWs1udwUolVy",
        "snbloqueado": true,
        "createdAt": "2021-11-08T16:34:51.000Z",
        "updatedAt": "2021-11-12T14:09:34.000Z"
      },
    "message": "Succesfully Updated User"
    }
    

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`


## Recuperar Contraseña

* **URL**

  http://localhost:8000/usuario/recuperarPassword

* **Metodo:**

  `POST`
  
*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "mail": "[String]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
    ```json
    {
      "message": "Hemos enviado un link a tu correo para que puedas cambiar tu contraseña"
    }
    

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`


## Actualizar Contraseña

* **URL**

  http://localhost:8000/usuario/actualizarPassword

* **Metodo:**

  `POST`
  
*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "mail": "[String]",
        "password": "[String]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
    ```json
    {
        "message": "Contraseña actualizada correctamente."
    }
    

* **Respuesta No Exitosa:**

  * **Codigo:** 400<br />
    **Contenido:** `Mail no valido o inexistente`

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`


# Hijos

## Crear Hijo

* **URL**

  http://localhost:8000/hijo/crear

* **Metodo:**

  `POST`

* **Data Params**: JSON

   ```json
   {
       "nombre": "[String]",
       "apellido": "[String]",
       "usuario": "[Integer]", //Id del Padre
       "grupoSanguineo": "[Integer]", //Id de Grupo Sanguineo
       "fechaNac": "[Date]", //Formato: yyyy-mm-dd
       "alergia": "[String]",
       "enfermedad": "[String]",
       "genero": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
      {
      "id": 6,
      "idUsuario": 3,
      "idGrupoSanguineo": 1,
      "nombre": "Hijo 1",
      "apellido": "Vaz",
      "fecNacimiento": "1993-11-03T00:00:00.000Z",
      "alergia": "sad",
      "enfermedad": "asd",
      "idGenero": "1",
      "updatedAt": "2021-11-12T14:14:16.315Z",
      "createdAt": "2021-11-12T14:14:16.315Z"
      }

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`


## Buscar Hijos por Padre

* **URL**

  http://localhost:8000/hijo/buscarHijos

* **Metodo:**

  `POST`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "idUsuario": "[Integer]" //Id del padre

    }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    [
      {
        "id": 1,
        "idUsuario": 3,
        "nombre": "Pepin",
        "apellido": "Pepon",
        "idGenero": 2,
        "fecNacimiento": "1990-11-03T00:00:00.000Z",
        "idGrupoSanguineo": 4,
        "alergia": "Muchas",
        "enfermedad": "uf",
        "createdAt": "2021-11-03T21:02:19.000Z",
        "updatedAt": "2021-11-03T21:13:46.000Z"
      }
    ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Actualizar Hijo

* **URL**

  http://localhost:8000/hijo/actualizar

* **Metodo:**

  `PUT`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[String]", //Id Hijo a actualizar
        "nombre": "[String]", //Nuevo nombre
        "apellido": "[String]", //Nuevo apellido
        "fecNacimiento": "[Date]", //Nueva FecNacimiento - Formato: yyyy-mm-dd
        "alergia": "[String]", //Nueva alergia
        "enfermedad": "[String]", //Nueva enfermedad
        "idgruposanguineo": "[Integer]", //Nuevo grupo sanguineo
        "idGenero": "[Integer]" //Nuevo Genero

    }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    [
      {
        "status": 200,
        "data": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": "2",
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": "4",
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.032Z"
        },
        "message": "Los datos han sido actualizados correctamente"
      }
    ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`



## Buscar Hijo

* **URL**

  http://localhost:8000/hijo/buscar/id

* **Metodo:**

  `POST`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[String]"

    }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    [
      {
          "id": 1,
          "idUsuario": 3,
          "nombre": "Pepin",
          "apellido": "Pepon",
          "idGenero": 2,
          "fecNacimiento": "1990-11-03T00:00:00.000Z",
          "idGrupoSanguineo": 4,
          "alergia": "Muchas",
          "enfermedad": "uf",
          "createdAt": "2021-11-03T21:02:19.000Z",
          "updatedAt": "2021-11-12T14:31:30.000Z"
      }
    ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`


# Grupo Sanguineo

## Crear Grupo Sanguineo

* **URL**

  http://localhost:8000/grupoSanguineo/crear

* **Metodo:**

  `POST`

* **Data Params**: JSON

   ```json
   {
       "grupo": "[String]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
      "id": 10,
      "descripcion": "AB+",
      "updatedAt": "2021-11-12T14:38:59.199Z",
      "createdAt": "2021-11-12T14:38:59.199Z"
    }

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Listar Grupos Sanguineos

* **URL**

  http://localhost:8000/grupoSanguineo/listar

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**:

   None

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    [
      {
          "id": 1,
          "descripcion": "A+",
          "createdAt": "2021-11-03T20:40:15.000Z",
          "updatedAt": "2021-11-03T20:40:15.000Z"
      },
      {
          "id": 2,
          "descripcion": "A-",
          "createdAt": "2021-11-03T20:40:22.000Z",
          "updatedAt": "2021-11-03T20:40:22.000Z"
      }
    ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`


## Buscar Grupo Sanguineo

* **URL**

  http://localhost:8000/grupoSanguineo/buscar/id

* **Metodo:**

  `GET`

*  **URL Parametros**

   **Requeridos:**
 
   None

* **Data Params**: JSON

   ```json
   {
       "id": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
        "id": 1,
        "descripcion": "A+",
        "createdAt": "2021-11-03T20:40:15.000Z",
        "updatedAt": "2021-11-03T20:40:15.000Z"
    }

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`


# Vacuna

## Crear Vacuna

* **URL**

  http://localhost:8000/vacuna/crear

* **Metodo:**

  `POST`

* **Data Params**: JSON

   ```json
   {
       "vacuna": "[String]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
      "id": 5,
      "descripcion": "COVID19 - Sputnik V",
      "updatedAt": "2021-11-12T14:44:03.614Z",
      "createdAt": "2021-11-12T14:44:03.614Z"
    }

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Listar Vacunas

* **URL**

  http://localhost:8000/grupoSanguineo/listar

* **Metodo:**

  `POST`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**:

   None

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    [
      {
          "id": 1,
          "descripcion": "COVID-19",
          "createdAt": "2021-11-03T21:16:26.000Z",
          "updatedAt": "2021-11-03T21:16:26.000Z"
      },
      {
          "id": 2,
          "descripcion": "Hepatitis B",
          "createdAt": "2021-11-03T21:16:43.000Z",
          "updatedAt": "2021-11-03T21:16:43.000Z"
      }
    ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`


## Buscar Vacuna

* **URL**

  http://localhost:8000/vacuna/buscar/id

* **Metodo:**

  `POST`

*  **URL Parametros**

   **Requeridos:**
 
   None

* **Data Params**: JSON

   ```json
   {
       "id": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
        "id": 1,
        "descripcion": "COVID-19",
        "createdAt": "2021-11-03T21:16:26.000Z",
        "updatedAt": "2021-11-03T21:16:26.000Z"
    }

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`


  
# Relacion Vacuna
## Crear Relacion Vacuna

* **URL**

  http://localhost:8000/relacion_vacuna/crear

* **Metodo:**

  `POST`
  
*  **URL Parametros**

  None

* **Data Params**

  * **Data Params**: JSON

   ```json
   {
       "hijo": "[Integer]",
       "vacuna": "[Integer]",
       "fecAplicacion": "[Date]",
       "lugarAplicacion": "[String]",
       "dosis": "[String]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
    "id": 4,
    "idHijo": 1,
    "idVacuna": 2,
    "fecAplicacion": "1995-11-03T00:00:00.000Z",
    "lugarAplicacion": "Lanús Oeste",
    "dosis": "1era dosis",
    "updatedAt": "2021-11-12T15:52:37.265Z",
    "createdAt": "2021-11-12T15:52:37.265Z"
}

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot POST`

## Listar Relaciones Vacunas

* **URL**

  http://localhost:8000/relacion_vacuna/listar

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
   None

* **Data Params**

  None

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
  ```json
  [
    {
          "id": 1,
          "idHijo": 1,
          "idVacuna": 1,
          "dosis": "2da dosis",
          "fecAplicacion": "2021-11-03T00:00:00.000Z",
          "lugarAplicacion": "Banfield",
          "createdAt": "2021-11-03T23:09:03.000Z",
          "updatedAt": "2021-11-03T23:22:32.000Z",
          "vacuna": {
              "id": 1,
              "descripcion": "COVID-19",
              "createdAt": "2021-11-03T21:16:26.000Z",
              "updatedAt": "2021-11-03T21:16:26.000Z"
          },
          "hijo": {
              "id": 1,
              "idUsuario": 3,
              "nombre": "Pepin",
              "apellido": "Pepon",
              "idGenero": 2,
              "fecNacimiento": "1990-11-03T00:00:00.000Z",
              "idGrupoSanguineo": 4,
              "alergia": "Muchas",
              "enfermedad": "uf",
              "createdAt": "2021-11-03T21:02:19.000Z",
              "updatedAt": "2021-11-12T14:31:30.000Z"
          }
      }
    ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Buscar Vacuna por ID

* **URL**

  http://localhost:8000/relacion_vacuna/buscar/id

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**

  * **Data Params**: JSON

   ```json
   {
       "id": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
  ```json
  [
      {
          "id": 1,
          "idHijo": 1,
          "idVacuna": 1,
          "dosis": "2da dosis",
          "fecAplicacion": "2021-11-03T00:00:00.000Z",
          "lugarAplicacion": "Banfield",
          "createdAt": "2021-11-03T23:09:03.000Z",
          "updatedAt": "2021-11-03T23:22:32.000Z",
          "vacuna": {
              "id": 1,
              "descripcion": "COVID-19",
              "createdAt": "2021-11-03T21:16:26.000Z",
              "updatedAt": "2021-11-03T21:16:26.000Z"
          },
          "hijo": {
              "id": 1,
              "idUsuario": 3,
              "nombre": "Pepin",
              "apellido": "Pepon",
              "idGenero": 2,
              "fecNacimiento": "1990-11-03T00:00:00.000Z",
              "idGrupoSanguineo": 4,
              "alergia": "Muchas",
              "enfermedad": "uf",
              "createdAt": "2021-11-03T21:02:19.000Z",
              "updatedAt": "2021-11-12T14:31:30.000Z"
          }
      }
  ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Eliminar Relacion Vacuna

* **URL**

  http://localhost:8000/relacion_vacuna/eliminarVacunacion

* **Metodo:**

  `DEL`
  
*  **URL Parametros**
  None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
      "status": 200,
      "data": 0,
      "message": "La vacuna aplicada ha sido borrado exitosamente"
    }
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Actualizar Relacion Vacuna

* **URL**

  http://localhost:8000/relacion_vacuna/actualizar

* **Metodo:**

  `PUT`
  
*  **URL Parametros**

   None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[Integer]",
        "lugarAplicacion": "[String]",
        "fecAplicacion": "[Date]",
        "idvacuna": "[Integer]",
        "dosis": "[String]",
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
    ```json
    {
      "status": 200,
      "data": {
          "id": 1,
          "idHijo": 1,
          "idVacuna": 1,
          "dosis": "2da dosis",
          "fecAplicacion": "2021-11-03T00:00:00.000Z",
          "lugarAplicacion": "Banfield",
          "createdAt": "2021-11-03T23:09:03.000Z",
          "updatedAt": "2021-11-03T23:22:32.000Z"
      },
      "message": "Los datos han sido actualizados correctamente"
    }
    

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`



## Relacion Vacuna - Buscar Por Hijo

* **URL**

  http://localhost:8000/relacion_vacuna/buscarPorHijo/id

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**

  * **Data Params**: JSON

   ```json
   {
       "id": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
  ```json
  [
    {
        "id": 1,
        "idHijo": 1,
        "idVacuna": 1,
        "dosis": "2da dosis",
        "fecAplicacion": "2021-11-03T00:00:00.000Z",
        "lugarAplicacion": "Banfield",
        "createdAt": "2021-11-03T23:09:03.000Z",
        "updatedAt": "2021-11-03T23:22:32.000Z",
        "vacuna": {
            "id": 1,
            "descripcion": "COVID-19",
            "createdAt": "2021-11-03T21:16:26.000Z",
            "updatedAt": "2021-11-03T21:16:26.000Z"
        },
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
  ]
    

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`





# Control Pediatrico
## Crear Control Pediatrico

* **URL**

  http://localhost:8000/controlPediatrico/crear

* **Metodo:**

  `POST`
  
*  **URL Parametros**

  None

* **Data Params**

  ```json
   {
       "hijo": "[Integer]",
       "fecControl": "[Date]",
       "peso": "[Float]",
       "altura": "[Float]",
       "diametroCabeza": "[Float]",
       "medicamentos": "[String]",
       "obsMedicamento": "[String]",
       "estudioMedico": "[String]",
       "obsEstudioMedico": "[String]",
       "observaciones": "[String]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
      "id": 5,
      "idHijo": 1,
      "fecControl": "2021-11-03T00:00:00.000Z",
      "peso": "72",
      "altura": "1.83",
      "diametroCabeza": "0",
      "medicamento": "Tafirol",
      "ObsMedicamento": "1 por dia",
      "estudioMedico": "Electrocardiograma",
      "ObsEstudioMedico": "Salio todo bien!",
      "observaciones": "Esta saludable, en buen estado.",
      "updatedAt": "2021-11-12T16:17:16.813Z",
      "createdAt": "2021-11-12T16:17:16.813Z"
    }

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot POST`

## Listar Controles Pediatricos

* **URL**

  http://localhost:8000/controlPediatrico/listar

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
   None

* **Data Params**

  None

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
  ```json
  [
    {
        "id": 1,
        "idHijo": 1,
        "fecControl": "2021-11-04T00:00:00.000Z",
        "peso": "80.00",
        "altura": "1.83",
        "diametroCabeza": "0.0000",
        "medicamento": "aspirina",
        "ObsMedicamento": "1 por dia",
        "estudioMedico": "Nignuno",
        "ObsEstudioMedico": "-",
        "observaciones": "Todo marcha bien para milhouse",
        "createdAt": "2021-11-03T23:30:51.000Z",
        "updatedAt": "2021-11-03T23:54:16.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    },
    {
        "id": 2,
        "idHijo": 1,
        "fecControl": "2021-11-03T00:00:00.000Z",
        "peso": "72.00",
        "altura": "2.00",
        "diametroCabeza": "0.0000",
        "medicamento": "Tafirol",
        "ObsMedicamento": null,
        "estudioMedico": "Electrocardiograma",
        "ObsEstudioMedico": null,
        "observaciones": "Esta saludable, en buen estado.",
        "createdAt": "2021-11-03T23:32:51.000Z",
        "updatedAt": "2021-11-03T23:32:51.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
    ]
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Buscar Control Pediatrico por ID

* **URL**

  http://localhost:8000/controlPediatrico/buscar/id

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**

  * **Data Params**: JSON

   ```json
   {
       "id": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
  ```json
  [
    {
        "id": 1,
        "idHijo": 1,
        "fecControl": "2021-11-04T00:00:00.000Z",
        "peso": "80.00",
        "altura": "1.83",
        "diametroCabeza": "0.0000",
        "medicamento": "aspirina",
        "ObsMedicamento": "1 por dia",
        "estudioMedico": "Nignuno",
        "ObsEstudioMedico": "-",
        "observaciones": "Todo marcha bien para milhouse",
        "createdAt": "2021-11-03T23:30:51.000Z",
        "updatedAt": "2021-11-03T23:54:16.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
  ]

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Eliminar Control Pediatrico

* **URL**

  http://localhost:8000/controlPediatrico/eliminarControl

* **Metodo:**

  `DEL`

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
  * **Contenido:** 
    ```json
    {
      "status": 200,
      "data": 0,
      "message": "El control ha sido borrado exitosamente"
    }
    ```

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
  * **Contenido:** `Cannot ERROR`

## Actualizar Control Pediatrico

* **URL**

  http://localhost:8000/controlPediatrico/actualizar

* **Metodo:**

  `PUT`

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "id": "[Integer]",
        "fecControl": "[Date]",
        "peso": "[Float]",
        "altura": "[Float]",
        "diametroCabeza": "[Float]",
        "medicamento": "[String]",
        "ObsMedicamento": "[String]",
        "estudioMedico": "[String]",
        "ObsEstudioMedico": "[String]",
        "observaciones": "[String]"
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
    ```json
    {
      "status": 200,
      "data": {
          "id": 1,
          "idHijo": 1,
          "fecControl": "2021-11-04T00:00:00.000Z",
          "peso": "80",
          "altura": "1.83",
          "diametroCabeza": "0",
          "medicamento": "aspirina",
          "ObsMedicamento": "1 por dia",
          "estudioMedico": "Nignuno",
          "ObsEstudioMedico": "-",
          "observaciones": "Todo marcha bien para milhouse",
          "createdAt": "2021-11-03T23:30:51.000Z",
          "updatedAt": "2021-11-12T16:34:10.756Z"
      },
      "message": "Los datos han sido actualizados correctamente"
    }
    

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`



## Control Pediatrico - Buscar Por Hijo

* **URL**

  http://localhost:8000/controlPediatrico/buscarPorHijo/id

* **Metodo:**

  `GET`
  
*  **URL Parametros**

   **Requeridos:**
 
    None

* **Data Params**

  * **Data Params**: JSON

   ```json
   {
       "id": "[Integer]"
   }

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
  ```json
  [
    [
    {
        "id": 1,
        "idHijo": 1,
        "fecControl": "2021-11-04T00:00:00.000Z",
        "peso": "80.00",
        "altura": "1.83",
        "diametroCabeza": "0.0000",
        "medicamento": "aspirina",
        "ObsMedicamento": "1 por dia",
        "estudioMedico": "Nignuno",
        "ObsEstudioMedico": "-",
        "observaciones": "Todo marcha bien para milhouse",
        "createdAt": "2021-11-03T23:30:51.000Z",
        "updatedAt": "2021-11-12T16:34:10.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    },
    {
        "id": 2,
        "idHijo": 1,
        "fecControl": "2021-11-03T00:00:00.000Z",
        "peso": "72.00",
        "altura": "2.00",
        "diametroCabeza": "0.0000",
        "medicamento": "Tafirol",
        "ObsMedicamento": null,
        "estudioMedico": "Electrocardiograma",
        "ObsEstudioMedico": null,
        "observaciones": "Esta saludable, en buen estado.",
        "createdAt": "2021-11-03T23:32:51.000Z",
        "updatedAt": "2021-11-03T23:32:51.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
  ]
    

* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`

## Control Pediatrico - Traer el ultimo para Percentiles por IdHijo

* **URL**

  http://localhost:8000/controlPediatrico/percentil
  
* **Metodo:**

  `GET`
  
*  **URL Parametros**

None

*  **Data Params**

   **Requeridos:**
 
   ```json
    {
        "idHijo": "[Integer]" //Id del hijo del cual se quiere traer el ultimo control
    }
    ```

* **Respuesta Exitosa:**

  * **Codigo:** 200 <br />
    **Contenido:** 
  ```json
  [
    {
        "MAX(`control`.`id`)": 8,
        "peso": "4.00",
        "altura": "0.72",
        "diametroCabeza": "28.0000",
        "hijo.id": 2,
        "hijo.idUsuario": 1,
        "hijo.nombre": "Jimena",
        "hijo.apellido": "Carlos",
        "hijo.idGenero": 2,
        "hijo.fecNacimiento": "2000-11-03T00:00:00.000Z",
        "hijo.idGrupoSanguineo": 1,
        "hijo.alergia": "Gatos, Lactosa",
        "hijo.enfermedad": "Ninguna",
        "hijo.createdAt": "2021-11-13T13:35:45.000Z",
        "hijo.updatedAt": "2021-11-13T13:35:45.000Z"
    }
  ]
    
* **Respuesta No Exitosa:**

  * **Codigo:** 404 NOT FOUND <br />
    **Contenido:** `Cannot ERROR`