'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('hijo', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idUsuario: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'usuario',
          key: 'id'
        }
      },
      nombre: {
        allowNull: false,
        type: Sequelize.STRING
      },
      apellido: {
        allowNull: false,
        type: Sequelize.STRING
      },
      idGenero: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      fecNacimiento: {
        allowNull: false,
        type: Sequelize.DATE
      },
      idGrupoSanguineo: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'grupo_sanguineo',
          key: 'id'
        }
      },
      alergia: {
        allowNull: true,
        type: Sequelize.STRING
      },
      enfermedad: {
        allowNull: true,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('hijo');
  }
};