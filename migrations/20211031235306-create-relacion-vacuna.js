"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("relacion_vacuna", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      idHijo: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "hijo",
          key: "id",
        },
      },
      idVacuna: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "vacuna",
          key: "id",
        },
      },
      dosis: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      fecAplicacion: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      lugarAplicacion: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("relacion_vacuna");
  },
};
