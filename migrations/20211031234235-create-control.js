'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('control', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idHijo: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'hijo',
          key: 'id'
        }
      },
      fecControl: {
        allowNull: false,
        type: Sequelize.DATE
      },
      peso: {
        type: Sequelize.DECIMAL
      },
      altura: {
        type: Sequelize.DECIMAL
      },
      diametroCabeza: {
        type: Sequelize.DECIMAL
      },
      medicamento: {
        type: Sequelize.STRING
      },
      ObsMedicamento: {
        type: Sequelize.STRING
      },
      estudioMedico: {
        type: Sequelize.STRING
      },
      ObsEstudioMedico: {
        type: Sequelize.STRING
      },
      observaciones: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('control');
  }
};