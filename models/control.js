'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class control extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      control.belongsTo(models.hijo, {
        as: "hijo",
        foreignKey: "idHijo",
      });
    }
  };
  control.init({
    idHijo: {
      allowNull: false,
      type:DataTypes.INTEGER
    },
    fecControl: {
      allowNull: false,
      type:DataTypes.DATE
    },
    peso: {
      allowNull: true,
      type:DataTypes.DECIMAL
    },
    altura: {
      allowNull: true,
      type:DataTypes.DECIMAL
    },
    diametroCabeza: {
      allowNull: true,
      type:DataTypes.DECIMAL
    },
    medicamento: {
      allowNull: true,
      type:DataTypes.STRING
    },
    ObsMedicamento: {
      allowNull: true,
      type:DataTypes.STRING
    },
    estudioMedico: {
      allowNull: true,
      type:DataTypes.STRING
    },
    ObsEstudioMedico: {
      allowNull: true,
      type:DataTypes.STRING
    },
    observaciones: {
      allowNull: true,
      type:DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'control',
  });
  return control;
};