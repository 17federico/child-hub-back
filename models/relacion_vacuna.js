'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class relacion_vacuna extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      relacion_vacuna.belongsTo(models.hijo, {
        as: "hijo",
        foreignKey: "idHijo",
      });

      relacion_vacuna.belongsTo(models.vacuna, {
        as: "vacuna",
        foreignKey: "idVacuna",
      });
    }
  };
  relacion_vacuna.init({
    idHijo: {
      allowNull: false,
      type:DataTypes.INTEGER
    },
    idVacuna: {
      allowNull: false,
      type:DataTypes.INTEGER
    },
    dosis: {
      allowNull: false,
      type:DataTypes.STRING
    },
    fecAplicacion: {
      allowNull: false,
      type:DataTypes.DATE
    },
    lugarAplicacion: {
      allowNull: false,
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'relacion_vacuna',
  });
  return relacion_vacuna;
};