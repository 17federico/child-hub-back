'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class usuario extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  usuario.init({
    dni: {
      allowNull: false,
      type:DataTypes.STRING
    },
    nombre: {
      allowNull: false,
      type:DataTypes.STRING
    },
    apellido: {
      allowNull: false,
      type:DataTypes.STRING
    },
    mail: {
      allowNull: false,
      type:DataTypes.STRING
    },
    telefono: {
      allowNull: false,
      type:DataTypes.STRING
    },
    password: {
      allowNull: false,
      type:DataTypes.STRING
    },
    snbloqueado: {
      allowNull: false,
      type:DataTypes.BOOLEAN,
      defaultValue: true
    },
  }, {
    sequelize,
    modelName: 'usuario',
  });
  return usuario;
};