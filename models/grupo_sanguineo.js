'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class grupo_sanguineo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  grupo_sanguineo.init({
    descripcion: {
      allowNull: false,
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'grupo_sanguineo',
  });
  return grupo_sanguineo;
};