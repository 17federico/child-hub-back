"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class hijo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      hijo.belongsTo(models.usuario, {
        as: "usuario",
        foreignKey: "idUsuario",
      });

      hijo.belongsTo(models.grupo_sanguineo, {
        as: "gruposanguineo",
        foreignKey: "idGrupoSanguineo",
      });

    }
  }
  hijo.init(
    {
      idUsuario: {
        allowNull: false,
        type:DataTypes.INTEGER
      },
      nombre: {
        allowNull: false,
        type:DataTypes.STRING
      },
      apellido: {
        allowNull: false,
        type:DataTypes.STRING
      },
      idGenero: {
        allowNull: false,
        type:DataTypes.INTEGER
      },
      fecNacimiento: {
        allowNull: false,
        type:DataTypes.DATE
      },
      idGrupoSanguineo: {
        allowNull: false,
        type:DataTypes.INTEGER
      },
      alergia: {
        allowNull: true,
        type:DataTypes.STRING
      },
      enfermedad: {
        allowNull: true,
        type:DataTypes.STRING
      },
    },
    {
      sequelize,
      modelName: "hijo",
    }
  );
  return hijo;
};
