CREATE DATABASE  IF NOT EXISTS `childhub` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `childhub`;
-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: childhub
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `controls`
--

DROP TABLE IF EXISTS `controls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `controls` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idHijo` int NOT NULL,
  `fecControl` date NOT NULL,
  `peso` decimal(10,0) DEFAULT NULL,
  `altura` decimal(10,0) DEFAULT NULL,
  `diametroCabeza` decimal(10,0) DEFAULT NULL,
  `medicamento` varchar(255) DEFAULT NULL,
  `ObsMedicamento` varchar(255) DEFAULT NULL,
  `estudioMedico` varchar(255) DEFAULT NULL,
  `ObsEstudioMedico` varchar(255) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idHijo` (`idHijo`),
  CONSTRAINT `controls_ibfk_1` FOREIGN KEY (`idHijo`) REFERENCES `hijos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controls`
--

LOCK TABLES `controls` WRITE;
/*!40000 ALTER TABLE `controls` DISABLE KEYS */;
/*!40000 ALTER TABLE `controls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_sanguineos`
--

DROP TABLE IF EXISTS `grupo_sanguineos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `grupo_sanguineos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo_sanguineos`
--

LOCK TABLES `grupo_sanguineos` WRITE;
/*!40000 ALTER TABLE `grupo_sanguineos` DISABLE KEYS */;
INSERT INTO `grupo_sanguineos` VALUES (1,'A+','2021-11-03 20:40:15','2021-11-03 20:40:15'),(2,'A-','2021-11-03 20:40:22','2021-11-03 20:40:22'),(3,'B-','2021-11-03 20:40:26','2021-11-03 20:40:26'),(4,'B+','2021-11-03 20:40:29','2021-11-03 20:40:29'),(5,'AB+','2021-11-03 20:40:33','2021-11-03 20:40:33'),(6,'AB-','2021-11-03 20:40:36','2021-11-03 20:40:36'),(7,'0+','2021-11-03 20:40:41','2021-11-03 20:40:41'),(8,'0-','2021-11-03 20:40:44','2021-11-03 20:40:44'),(9,'Desconocido','2021-11-03 20:42:49','2021-11-03 20:42:49');
/*!40000 ALTER TABLE `grupo_sanguineos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hijos`
--

DROP TABLE IF EXISTS `hijos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hijos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idUsuario` int NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `idGenero` int NOT NULL,
  `fecNacimiento` datetime NOT NULL,
  `idGrupoSanguineo` int NOT NULL,
  `alergia` varchar(255) DEFAULT NULL,
  `enfermedad` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idUsuario` (`idUsuario`),
  KEY `idGrupoSanguineo` (`idGrupoSanguineo`),
  CONSTRAINT `hijos_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `hijos_ibfk_2` FOREIGN KEY (`idGrupoSanguineo`) REFERENCES `grupo_sanguineos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hijos`
--

LOCK TABLES `hijos` WRITE;
/*!40000 ALTER TABLE `hijos` DISABLE KEYS */;
INSERT INTO `hijos` VALUES (8,13,'Manuel','Gimenez',1,'2020-12-13 00:00:00',1,'','','2021-12-13 21:16:04','2021-12-13 21:16:04');
/*!40000 ALTER TABLE `hijos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relacion_vacunas`
--

DROP TABLE IF EXISTS `relacion_vacunas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `relacion_vacunas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idHijo` int NOT NULL,
  `idVacuna` int NOT NULL,
  `dosis` varchar(255) NOT NULL,
  `fecAplicacion` date NOT NULL,
  `lugarAplicacion` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idHijo` (`idHijo`),
  KEY `idVacuna` (`idVacuna`),
  CONSTRAINT `relacion_vacunas_ibfk_1` FOREIGN KEY (`idHijo`) REFERENCES `hijos` (`id`),
  CONSTRAINT `relacion_vacunas_ibfk_2` FOREIGN KEY (`idVacuna`) REFERENCES `vacunas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relacion_vacunas`
--

LOCK TABLES `relacion_vacunas` WRITE;
/*!40000 ALTER TABLE `relacion_vacunas` DISABLE KEYS */;
/*!40000 ALTER TABLE `relacion_vacunas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequelizemeta`
--

DROP TABLE IF EXISTS `sequelizemeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sequelizemeta` (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequelizemeta`
--

LOCK TABLES `sequelizemeta` WRITE;
/*!40000 ALTER TABLE `sequelizemeta` DISABLE KEYS */;
INSERT INTO `sequelizemeta` VALUES ('20211031234208-create-usuario.js'),('20211031234216-create-grupo-sanguineo.js'),('20211031234225-create-hijo.js'),('20211031234235-create-control.js'),('20211031235255-create-vacuna.js'),('20211031235306-create-relacion-vacuna.js');
/*!40000 ALTER TABLE `sequelizemeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dni` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `snbloqueado` int NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (13,'34123642','Jose','Gimenez','93.vazquezmartin@gmail.com','1133464033','$2a$08$CVfQfAxbjJ9aroUuByD4D.KYi9XTvzzJTheyKTY/HB3nBuuozy9C2',1,'2021-12-13 21:15:13','2021-12-13 21:15:13');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacunas`
--

DROP TABLE IF EXISTS `vacunas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacunas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacunas`
--

LOCK TABLES `vacunas` WRITE;
/*!40000 ALTER TABLE `vacunas` DISABLE KEYS */;
INSERT INTO `vacunas` VALUES (1,'COVID-19','2021-11-03 21:16:26','2021-11-03 21:16:26'),(2,'Hepatitis B','2021-11-03 21:16:43','2021-11-03 21:16:43'),(3,'Paperas','2021-11-03 21:16:49','2021-11-03 21:16:49'),(4,'Gripe','2021-11-03 21:16:56','2021-11-03 21:16:56'),(5,'BCG','2021-12-13 18:12:04','2021-12-13 18:12:04'),(6,'Quintuple Pentavalente','2021-12-13 18:12:29','2021-12-13 18:12:29'),(7,'Polio IPV','2021-12-13 18:12:42','2021-12-13 18:12:42'),(8,'Rotavirus','2021-12-13 18:12:59','2021-12-13 18:12:59'),(9,'Meningococo','2021-12-13 18:13:10','2021-12-13 18:13:10'),(10,'Hepatitis A','2021-12-13 18:13:20','2021-12-13 18:13:20'),(11,'Varicela','2021-12-13 18:13:45','2021-12-13 18:13:45'),(12,'Triple Viral','2021-12-13 18:14:02','2021-12-13 18:14:02');
/*!40000 ALTER TABLE `vacunas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'childhub'
--

--
-- Dumping routines for database 'childhub'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-13 18:20:42
