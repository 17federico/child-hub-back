# Introduction
In order to develop a backend with microservices for Child-Hub, in this file you will find each one of them, indicating its inputs and outputs.

To make this project work, you first need to create a MySQL database with the name 'ChildHub'.

To start this project, you must run this commands:

1) Open MySQL WorkBrench. Execute the script "dbChildHub.sql" that has some data example. Check out the file "config.json" that is on the config folder of this project. In case you have a username/password different from the one of this file, you must change it and after that run the commnad: **npm start**.
2) **npm install**
3) **npm start**

# Users
## Create Users

* **URL**

  http://localhost:8000/usuario/crear

* **Method:**

  `POST`
  
*  **URL Params**

   None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "dni": "[String]",
        "mail": "[String]",
        "nombre": "[String]",
        "apellido": "[String]",
        "password": "[String]",
        "telefono":"[String]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    { 
         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzY3MjQ5NTMsImV4cCI6MTYzNjgxMTM1M30.a2IWLSp7pcsnT1hse0EwPD3seZojWQx_ON3gM7GbOPE",
    "usuario": {
        "snbloqueado": true,
        "id": 12,
        "dni": "32000000",
        "mail": "frca@inworx.com",
        "nombre": "Lol",
        "apellido": "2o0",
        "password": "$2a$08$MGFuC4iSaAsU9MiVeJVwfe7xPm6PG0RCcM9Bi2QgwgFs5YMjBoMZe",
        "telefono": "213213213",
        "updatedAt": "2021-11-12T13:49:13.574Z",
        "createdAt": "2021-11-12T13:49:13.574Z"
      }
    }
    ```

* **Error Response:**

  * **Code:** 202 <br />
  * **Content:** `Email ya existente`

    * **Code:** 202 <br />
  * **Content:** `DNI ya existente`

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot POST`

## List Users

* **URL**

  http://localhost:8000/usuario/listar

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    [
        {
        "id": 3,
        "dni": "37762997",
        "nombre": "Mart",
        "apellido": "Vaz",
        "mail": "mrv@ma.com",
        "telefono": "1122",
        "password": "$2a$08$sN5mkflAjH85MwlrciFFy.BEBhouz3RfczZaz7fxjphHi8igVKGQC",
        "snbloqueado": true,
        "createdAt": "2021-11-03T18:55:07.000Z",
        "updatedAt": "2021-11-03T19:11:38.000Z"
        }
    ]
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Find User by DNI

* **URL**

  http://localhost:8000/usuario/buscar

* **Method:**

  `GET`
  
*  **URL Params**

  None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "dni": "[String]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
        "id": 5,
        "dni": "4354",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "hola@lalo.com",
        "telefono": "213213213",
        "password": "$2a$08$gu9Y72I/4eGdoLrDLi.a5eKW2eRPk8Ht57uGfEqylhz9Ai8kgQfoi",
        "snbloqueado": true,
        "createdAt": "2021-11-03T19:35:58.000Z",
        "updatedAt": "2021-11-03T20:32:57.000Z"
    }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Find Users by ID

* **URL**

  http://localhost:8000/usuario/buscar/id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
    None

* **Data Params**

     ```json
    {
        "id": "[Integer]"
    }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
        "id": 5,
        "dni": "4354",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "hola@lalo.com",
        "telefono": "213213213",
        "password": "$2a$08$gu9Y72I/4eGdoLrDLi.a5eKW2eRPk8Ht57uGfEqylhz9Ai8kgQfoi",
        "snbloqueado": true,
        "createdAt": "2021-11-03T19:35:58.000Z",
        "updatedAt": "2021-11-03T20:32:57.000Z"
    }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Loggin User

* **URL**

  http://localhost:8000/usuario/loguear

* **Method:**

  `POST`
  
*  **URL Params**

   None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "email": "[String]",
        "password": "[String]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzY3MjU2NTAsImV4cCI6MTYzNjgxMjA1MH0.QBjtzDDicgUlrpDTm2eT4mrF0LQEKnCCAoSV13ERQIs",
    "usuario": {
        "id": 13,
        "dni": "32000000",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "frcac@inworx.com",
        "telefono": "213213213",
        "password": "$2a$08$Yq8WwQ6DnpQO6m9ZjtHenOIPIH.e5.584DCPsX3P.9ZgQUqxlRBda",
        "snbloqueado": true,
        "createdAt": "2021-11-12T14:00:24.000Z",
        "updatedAt": "2021-11-12T14:00:24.000Z"
      }
    }
    

* **Error Response:**
* **Code:** 202 <br />
    **Content:** `Invalid Username`

* **Code:** 203 <br />
    **Content:** `Invalid username or password`

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`



## Update User

* **URL**

  http://localhost:8000/usuario/actualizar

* **Method:**

  `PUT`
  
*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[String]", //Id of the user that is gonna be updated
        "dni": "[String]", //New DNI
        "nombre": "[String]", //New name
        "apellido": "[String]", //New surname
        "telefono": "[String]", //New phone
        "mail": "[String]" //New mail

    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
    "status": 200,
    "data": {
        "id": 10,
        "dni": "30000000",
        "nombre": "Lol",
        "apellido": "2o0",
        "mail": "frcac@inworx.com",
        "telefono": "213213213",
        "password": "$2a$08$uQSNUIOhpnAy7xRtKuT4XuMlmcci3qykz2hZZzvl6aWs1udwUolVy",
        "snbloqueado": true,
        "createdAt": "2021-11-08T16:34:51.000Z",
        "updatedAt": "2021-11-12T14:09:34.000Z"
      },
    "message": "Succesfully Updated User"
    }
    

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`


## Recovery Password

* **URL**

  http://localhost:8000/usuario/recuperarPassword

* **Method:**

  `POST`
  
*  **Data Params**

   **Required:**
 
   ```json
    {
        "mail": "[String]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "message": "Hemos enviado un link a tu correo para que puedas cambiar tu contraseña"
    }
    

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`


## Update Password

* **URL**

  http://localhost:8000/usuario/actualizarPassword

* **Method:**

  `POST`
  
*  **Data Params**

   **Required:**
 
   ```json
    {
        "mail": "[String]",
        "password": "[String]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
        "message": "Contraseña actualizada correctamente."
    }
    

* **Error Response:**

  * **Code:** 400<br />
    **Content:** `Mail no valido o inexistente`

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`



# Children

## Create Child

* **URL**

  http://localhost:8000/hijo/crear

* **Method:**

  `POST`

* **Data Params**: JSON

   ```json
   {
       "nombre": "[String]",
       "apellido": "[String]",
       "usuario": "[Integer]", //Father ID
       "grupoSanguineo": "[Integer]", //Blood Group ID
       "fechaNac": "[Date]", //Format: yyyy-mm-dd
       "alergia": "[String]",
       "enfermedad": "[String]",
       "genero": "[Integer]"
   }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
      {
      "id": 6,
      "idUsuario": 3,
      "idGrupoSanguineo": 1,
      "nombre": "Hijo 1",
      "apellido": "Vaz",
      "fecNacimiento": "1993-11-03T00:00:00.000Z",
      "alergia": "sad",
      "enfermedad": "asd",
      "idGenero": "1",
      "updatedAt": "2021-11-12T14:14:16.315Z",
      "createdAt": "2021-11-12T14:14:16.315Z"
      }

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`


## Find Children by Father

* **URL**

  http://localhost:8000/hijo/buscarHijos

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
    None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "idUsuario": "[Integer]" //Father ID

    }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    [
      {
        "id": 1,
        "idUsuario": 3,
        "nombre": "Pepin",
        "apellido": "Pepon",
        "idGenero": 2,
        "fecNacimiento": "1990-11-03T00:00:00.000Z",
        "idGrupoSanguineo": 4,
        "alergia": "Muchas",
        "enfermedad": "uf",
        "createdAt": "2021-11-03T21:02:19.000Z",
        "updatedAt": "2021-11-03T21:13:46.000Z"
      }
    ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Update Child

* **URL**

  http://localhost:8000/hijo/actualizar

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
    None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[String]", //Son ID
        "nombre": "[String]", //New name
        "apellido": "[String]", //New surname
        "fecNacimiento": "[Date]", //New Born Date - Format: yyyy-mm-dd
        "alergia": "[String]", //New allergy
        "enfermedad": "[String]", //New sickness
        "idgruposanguineo": "[Integer]", //New Blood Group
        "idGenero": "[Integer]" //New Gender

    }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    [
      {
        "status": 200,
        "data": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": "2",
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": "4",
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.032Z"
        },
        "message": "Los datos han sido actualizados correctamente"
      }
    ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`



## Find Child

* **URL**

  http://localhost:8000/hijo/buscar/id

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
   None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    [
      {
          "id": 1,
          "idUsuario": 3,
          "nombre": "Pepin",
          "apellido": "Pepon",
          "idGenero": 2,
          "fecNacimiento": "1990-11-03T00:00:00.000Z",
          "idGrupoSanguineo": 4,
          "alergia": "Muchas",
          "enfermedad": "uf",
          "createdAt": "2021-11-03T21:02:19.000Z",
          "updatedAt": "2021-11-12T14:31:30.000Z"
      }
    ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`


# Blood Group

## Create Blood Group

* **URL**

  http://localhost:8000/grupoSanguineo/crear

* **Method:**

  `POST`

* **Data Params**: JSON

   ```json
   {
       "grupo": "[String]"
   }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
      "id": 10,
      "descripcion": "AB+",
      "updatedAt": "2021-11-12T14:38:59.199Z",
      "createdAt": "2021-11-12T14:38:59.199Z"
    }

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## List Blood Group

* **URL**

  http://localhost:8000/grupoSanguineo/listar

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
    None

* **Data Params**:

   None

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    [
      {
          "id": 1,
          "descripcion": "A+",
          "createdAt": "2021-11-03T20:40:15.000Z",
          "updatedAt": "2021-11-03T20:40:15.000Z"
      },
      {
          "id": 2,
          "descripcion": "A-",
          "createdAt": "2021-11-03T20:40:22.000Z",
          "updatedAt": "2021-11-03T20:40:22.000Z"
      }
    ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`


## Find Blood Group

* **URL**

  http://localhost:8000/grupoSanguineo/buscar/id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**
 
   None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
        "id": 1,
        "descripcion": "A+",
        "createdAt": "2021-11-03T20:40:15.000Z",
        "updatedAt": "2021-11-03T20:40:15.000Z"
    }

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`


# Vaccine

## Create Vaccine

* **URL**

  http://localhost:8000/vacuna/crear

* **Method:**

  `POST`

* **Data Params**: JSON

   ```json
   {
       "vacuna": "[String]"
   }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
      "id": 5,
      "descripcion": "COVID19 - Sputnik V",
      "updatedAt": "2021-11-12T14:44:03.614Z",
      "createdAt": "2021-11-12T14:44:03.614Z"
    }

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## List Vaccines

* **URL**

  http://localhost:8000/grupoSanguineo/listar

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
    None

* **Data Params**:

   None

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    [
      {
          "id": 1,
          "descripcion": "COVID-19",
          "createdAt": "2021-11-03T21:16:26.000Z",
          "updatedAt": "2021-11-03T21:16:26.000Z"
      },
      {
          "id": 2,
          "descripcion": "Hepatitis B",
          "createdAt": "2021-11-03T21:16:43.000Z",
          "updatedAt": "2021-11-03T21:16:43.000Z"
      }
    ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`


## Find Vaccine

* **URL**

  http://localhost:8000/vacuna/buscar/id

* **Method:**

  `POST`

*  **URL Params**

   **Required:**
 
   None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
        "id": 1,
        "descripcion": "COVID-19",
        "createdAt": "2021-11-03T21:16:26.000Z",
        "updatedAt": "2021-11-03T21:16:26.000Z"
    }

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`


  
# Relate Vaccine
## Create Relate Vaccine

* **URL**

  http://localhost:8000/relacion_vacuna/crear

* **Method:**

  `POST`
  
*  **URL Params**

  None

* **Data Params**

  * **Data Params**: JSON

   ```json
   {
       "hijo": "[Integer]",
       "vacuna": "[Integer]",
       "fecAplicacion": "[Date]",
       "lugarAplicacion": "[String]",
       "dosis": "[String]"
   }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
    "id": 4,
    "idHijo": 1,
    "idVacuna": 2,
    "fecAplicacion": "1995-11-03T00:00:00.000Z",
    "lugarAplicacion": "Lanús Oeste",
    "dosis": "1era dosis",
    "updatedAt": "2021-11-12T15:52:37.265Z",
    "createdAt": "2021-11-12T15:52:37.265Z"
}

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot POST`

## List Relate Vaccine

* **URL**

  http://localhost:8000/relacion_vacuna/listar

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
  ```json
  [
    {
          "id": 1,
          "idHijo": 1,
          "idVacuna": 1,
          "dosis": "2da dosis",
          "fecAplicacion": "2021-11-03T00:00:00.000Z",
          "lugarAplicacion": "Banfield",
          "createdAt": "2021-11-03T23:09:03.000Z",
          "updatedAt": "2021-11-03T23:22:32.000Z",
          "vacuna": {
              "id": 1,
              "descripcion": "COVID-19",
              "createdAt": "2021-11-03T21:16:26.000Z",
              "updatedAt": "2021-11-03T21:16:26.000Z"
          },
          "hijo": {
              "id": 1,
              "idUsuario": 3,
              "nombre": "Pepin",
              "apellido": "Pepon",
              "idGenero": 2,
              "fecNacimiento": "1990-11-03T00:00:00.000Z",
              "idGrupoSanguineo": 4,
              "alergia": "Muchas",
              "enfermedad": "uf",
              "createdAt": "2021-11-03T21:02:19.000Z",
              "updatedAt": "2021-11-12T14:31:30.000Z"
          }
      }
    ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Find Relate Vaccine by ID

* **URL**

  http://localhost:8000/relacion_vacuna/buscar/id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
    None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
  ```json
  [
      {
          "id": 1,
          "idHijo": 1,
          "idVacuna": 1,
          "dosis": "2da dosis",
          "fecAplicacion": "2021-11-03T00:00:00.000Z",
          "lugarAplicacion": "Banfield",
          "createdAt": "2021-11-03T23:09:03.000Z",
          "updatedAt": "2021-11-03T23:22:32.000Z",
          "vacuna": {
              "id": 1,
              "descripcion": "COVID-19",
              "createdAt": "2021-11-03T21:16:26.000Z",
              "updatedAt": "2021-11-03T21:16:26.000Z"
          },
          "hijo": {
              "id": 1,
              "idUsuario": 3,
              "nombre": "Pepin",
              "apellido": "Pepon",
              "idGenero": 2,
              "fecNacimiento": "1990-11-03T00:00:00.000Z",
              "idGrupoSanguineo": 4,
              "alergia": "Muchas",
              "enfermedad": "uf",
              "createdAt": "2021-11-03T21:02:19.000Z",
              "updatedAt": "2021-11-12T14:31:30.000Z"
          }
      }
  ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Delete Relate Vaccine

* **URL**

  http://localhost:8000/relacion_vacuna/eliminarVacunacion

* **Method:**

  `DEL`
  
*  **URL Params**
  None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
      "status": 200,
      "data": 0,
      "message": "La vacuna aplicada ha sido borrado exitosamente"
    }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Update Relate Vaccine

* **URL**

  http://localhost:8000/relacion_vacuna/actualizar

* **Method:**

  `PUT`
  
*  **URL Params**

   None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]",
        "lugarAplicacion": "[String]",
        "fecAplicacion": "[Date]",
        "idvacuna": "[Integer]",
        "dosis": "[String]",
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "status": 200,
      "data": {
          "id": 1,
          "idHijo": 1,
          "idVacuna": 1,
          "dosis": "2da dosis",
          "fecAplicacion": "2021-11-03T00:00:00.000Z",
          "lugarAplicacion": "Banfield",
          "createdAt": "2021-11-03T23:09:03.000Z",
          "updatedAt": "2021-11-03T23:22:32.000Z"
      },
      "message": "Los datos han sido actualizados correctamente"
    }
    

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`



## Relate Vaccine - Find by Child

* **URL**

  http://localhost:8000/relacion_vacuna/buscarPorHijo/id

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
    None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
  ```json
  [
    {
        "id": 1,
        "idHijo": 1,
        "idVacuna": 1,
        "dosis": "2da dosis",
        "fecAplicacion": "2021-11-03T00:00:00.000Z",
        "lugarAplicacion": "Banfield",
        "createdAt": "2021-11-03T23:09:03.000Z",
        "updatedAt": "2021-11-03T23:22:32.000Z",
        "vacuna": {
            "id": 1,
            "descripcion": "COVID-19",
            "createdAt": "2021-11-03T21:16:26.000Z",
            "updatedAt": "2021-11-03T21:16:26.000Z"
        },
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
  ]
    

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`





# Paediatric Control
## Create Paediatric Control

* **URL**

  http://localhost:8000/controlPediatrico/crear

* **Method:**

  `POST`
  
*  **URL Params**

  None

* **Data Params**

  ```json
   {
       "hijo": "[Integer]",
       "fecControl": "[Date]",
       "peso": "[Float]",
       "altura": "[Float]",
       "diametroCabeza": "[Float]",
       "medicamentos": "[String]",
       "obsMedicamento": "[String]",
       "estudioMedico": "[String]",
       "obsEstudioMedico": "[String]",
       "observaciones": "[String]"
   }

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
      "id": 5,
      "idHijo": 1,
      "fecControl": "2021-11-03T00:00:00.000Z",
      "peso": "72",
      "altura": "1.83",
      "diametroCabeza": "0",
      "medicamento": "Tafirol",
      "ObsMedicamento": "1 por dia",
      "estudioMedico": "Electrocardiograma",
      "ObsEstudioMedico": "Salio todo bien!",
      "observaciones": "Esta saludable, en buen estado.",
      "updatedAt": "2021-11-12T16:17:16.813Z",
      "createdAt": "2021-11-12T16:17:16.813Z"
    }

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot POST`

## List Paediatric Control

* **URL**

  http://localhost:8000/controlPediatrico/listar

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
  ```json
  [
    {
        "id": 1,
        "idHijo": 1,
        "fecControl": "2021-11-04T00:00:00.000Z",
        "peso": "80.00",
        "altura": "1.83",
        "diametroCabeza": "0.0000",
        "medicamento": "aspirina",
        "ObsMedicamento": "1 por dia",
        "estudioMedico": "Nignuno",
        "ObsEstudioMedico": "-",
        "observaciones": "Todo marcha bien para milhouse",
        "createdAt": "2021-11-03T23:30:51.000Z",
        "updatedAt": "2021-11-03T23:54:16.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    },
    {
        "id": 2,
        "idHijo": 1,
        "fecControl": "2021-11-03T00:00:00.000Z",
        "peso": "72.00",
        "altura": "2.00",
        "diametroCabeza": "0.0000",
        "medicamento": "Tafirol",
        "ObsMedicamento": null,
        "estudioMedico": "Electrocardiograma",
        "ObsEstudioMedico": null,
        "observaciones": "Esta saludable, en buen estado.",
        "createdAt": "2021-11-03T23:32:51.000Z",
        "updatedAt": "2021-11-03T23:32:51.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
    ]
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Find Paediatric Control by ID

* **URL**

  http://localhost:8000/controlPediatrico/buscar/id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
    None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
  ```json
  [
    {
        "id": 1,
        "idHijo": 1,
        "fecControl": "2021-11-04T00:00:00.000Z",
        "peso": "80.00",
        "altura": "1.83",
        "diametroCabeza": "0.0000",
        "medicamento": "aspirina",
        "ObsMedicamento": "1 por dia",
        "estudioMedico": "Nignuno",
        "ObsEstudioMedico": "-",
        "observaciones": "Todo marcha bien para milhouse",
        "createdAt": "2021-11-03T23:30:51.000Z",
        "updatedAt": "2021-11-03T23:54:16.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
  ]

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Delete Paediatric Control

* **URL**

  http://localhost:8000/controlPediatrico/eliminarControl

* **Method:**

  `DEL`

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** 
    ```json
    {
      "status": 200,
      "data": 0,
      "message": "El control ha sido borrado exitosamente"
    }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
  * **Content:** `Cannot ERROR`

## Update Paediatric Control

* **URL**

  http://localhost:8000/controlPediatrico/actualizar

* **Method:**

  `PUT`

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]",
        "fecControl": "[Date]",
        "peso": "[Float]",
        "altura": "[Float]",
        "diametroCabeza": "[Float]",
        "medicamento": "[String]",
        "ObsMedicamento": "[String]",
        "estudioMedico": "[String]",
        "ObsEstudioMedico": "[String]",
        "observaciones": "[String]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "status": 200,
      "data": {
          "id": 1,
          "idHijo": 1,
          "fecControl": "2021-11-04T00:00:00.000Z",
          "peso": "80",
          "altura": "1.83",
          "diametroCabeza": "0",
          "medicamento": "aspirina",
          "ObsMedicamento": "1 por dia",
          "estudioMedico": "Nignuno",
          "ObsEstudioMedico": "-",
          "observaciones": "Todo marcha bien para milhouse",
          "createdAt": "2021-11-03T23:30:51.000Z",
          "updatedAt": "2021-11-12T16:34:10.756Z"
      },
      "message": "Los datos han sido actualizados correctamente"
    }
    

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`



## Paediatric Control - Find by Child

* **URL**

  http://localhost:8000/controlPediatrico/buscarPorHijo/id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
    None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "id": "[Integer]"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
  ```json
  [
    [
    {
        "id": 1,
        "idHijo": 1,
        "fecControl": "2021-11-04T00:00:00.000Z",
        "peso": "80.00",
        "altura": "1.83",
        "diametroCabeza": "0.0000",
        "medicamento": "aspirina",
        "ObsMedicamento": "1 por dia",
        "estudioMedico": "Nignuno",
        "ObsEstudioMedico": "-",
        "observaciones": "Todo marcha bien para milhouse",
        "createdAt": "2021-11-03T23:30:51.000Z",
        "updatedAt": "2021-11-12T16:34:10.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    },
    {
        "id": 2,
        "idHijo": 1,
        "fecControl": "2021-11-03T00:00:00.000Z",
        "peso": "72.00",
        "altura": "2.00",
        "diametroCabeza": "0.0000",
        "medicamento": "Tafirol",
        "ObsMedicamento": null,
        "estudioMedico": "Electrocardiograma",
        "ObsEstudioMedico": null,
        "observaciones": "Esta saludable, en buen estado.",
        "createdAt": "2021-11-03T23:32:51.000Z",
        "updatedAt": "2021-11-03T23:32:51.000Z",
        "hijo": {
            "id": 1,
            "idUsuario": 3,
            "nombre": "Pepin",
            "apellido": "Pepon",
            "idGenero": 2,
            "fecNacimiento": "1990-11-03T00:00:00.000Z",
            "idGrupoSanguineo": 4,
            "alergia": "Muchas",
            "enfermedad": "uf",
            "createdAt": "2021-11-03T21:02:19.000Z",
            "updatedAt": "2021-11-12T14:31:30.000Z"
        }
    }
  ]
    

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`


## Paediatric Control - Bring the last one for Percentils by IdHijo

* **URL**

  http://localhost:8000/controlPediatrico/percentil
  
* **Method:**

  `GET`
  
*  **URL Params**

None

*  **Data Params**

   **Required:**
 
   ```json
    {
        "idHijo": "[Integer]" //Son Id of which you want to bring the last control
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
  ```json
  [
    {
        "MAX(`control`.`id`)": 8,
        "peso": "4.00",
        "altura": "0.72",
        "diametroCabeza": "28.0000",
        "hijo.id": 2,
        "hijo.idUsuario": 1,
        "hijo.nombre": "Jimena",
        "hijo.apellido": "Carlos",
        "hijo.idGenero": 2,
        "hijo.fecNacimiento": "2000-11-03T00:00:00.000Z",
        "hijo.idGrupoSanguineo": 1,
        "hijo.alergia": "Gatos, Lactosa",
        "hijo.enfermedad": "Ninguna",
        "hijo.createdAt": "2021-11-13T13:35:45.000Z",
        "hijo.updatedAt": "2021-11-13T13:35:45.000Z"
    }
  ]
    
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `Cannot ERROR`