/* Controllers */

const usuarioController = require('../controllers/usuario');
const hijoController = require('../controllers/hijo');
const gruposanguineoController = require('../controllers/grupo_sanguineo');
const vacunaController = require('../controllers/vacuna');
const relacionvacunaController = require('../controllers/relacion_vacuna');
const controlPediatricoController = require('../controllers/control');

var Authorization = require('../config/authorization');

module.exports = (app) => {
   app.get('/api', (req, res) => res.status(200).send ({
        message: 'Example project did not give you access to the api web services',
   }));
   app.post('/usuario/crear', usuarioController.create);
   app.get('/usuario/listar', usuarioController.list);
   app.get('/usuario/buscar', usuarioController.find);
   app.get('/usuario/buscar/id', usuarioController.findOne);
   app.post('/usuario/loguear', usuarioController.loginUser);
   app.put('/usuario/actualizar',Authorization, usuarioController.update);
   app.post('/usuario/actualizarPassword', Authorization,usuarioController.updatePassword);
   app.post('/usuario/recuperarPassword', usuarioController.recoveryPassword); 

   app.post('/hijo/crear', Authorization,hijoController.create);
   app.post('/hijo/buscarHijos', Authorization,hijoController.hijosPorPadre);
   app.put('/hijo/actualizar', Authorization,hijoController.update);
   app.post('/hijo/buscar/id', Authorization,hijoController.findOne);

   app.post('/grupoSanguineo/crear', gruposanguineoController.create);
   app.get('/grupoSanguineo/listar', gruposanguineoController.list);
   app.get('/grupoSanguineo/buscar/id', gruposanguineoController.find);

   app.post('/vacuna/crear', vacunaController.create);
   app.post('/vacuna/listar', vacunaController.list);
   app.post('/vacuna/buscar/id', vacunaController.find);

   app.post('/relacion_vacuna/crear', Authorization,relacionvacunaController.create);
   app.get('/relacion_vacuna/listar', Authorization,relacionvacunaController.list);
   app.get('/relacion_vacuna/buscar/id', Authorization,relacionvacunaController.find);
   app.delete('/relacion_vacuna/eliminarVacunacion', Authorization,relacionvacunaController.deleteVacunacion);
   app.put('/relacion_vacuna/actualizar', Authorization,relacionvacunaController.update);
   app.post('/relacion_vacuna/buscarPorHijo/id', Authorization,relacionvacunaController.findPorHijo);

   app.post('/controlPediatrico/crear', Authorization,controlPediatricoController.create);
   app.get('/controlPediatrico/listar', Authorization,controlPediatricoController.list);
   app.get('/controlPediatrico/buscar/id', Authorization,controlPediatricoController.find);
   app.delete('/controlPediatrico/eliminarControl', Authorization,controlPediatricoController.deleteControl);
   app.put('/controlPediatrico/actualizar', Authorization,controlPediatricoController.update);
   app.post('/controlPediatrico/buscarPorHijo/id', Authorization,controlPediatricoController.findPorHijo);
   app.get('/controlPediatrico/percentil', Authorization,controlPediatricoController.findAll);

};