const Sequelize     = require('sequelize');
const grupo_sanguineo       = require('../models').grupo_sanguineo;

module.exports = {
 create(req, res) {
    return grupo_sanguineo
        .create ({
            descripcion:req.body.grupo
             
        })
        .then(grupo_sanguineo => res.status(200).send(grupo_sanguineo))
        .catch(error => res.status(400).send(error))
 },
 list(_, res) {
     return grupo_sanguineo.findAll({})
        .then(grupo_sanguineo => res.status(200).send(grupo_sanguineo))
        .catch(error => res.status(400).send(error))
 },
 find (req, res) {
     return grupo_sanguineo.findAll({
         where: {
             id: req.body.id,
         }
     })
     .then(grupo_sanguineo => res.status(200).send(grupo_sanguineo))
     .catch(error => res.status(400).send(error))
  },
};