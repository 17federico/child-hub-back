"use strict";
const nodemailer = require("nodemailer");


module.exports = {

  async sendEmailToRecoveryPass(email, token) {

    // let linkRecuperador = `http://localhost:3000/recoverypassword/?token=${token}&mail=${email}`
    let linkRecuperador = `http://localhost:3000/changePassword/?token=${token}&mail=${email}`
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      secure: false, //SSL
      port: 25,
      auth: {
        user: 'child.hub.api2021@gmail.com',
        pass: '2021child'
      },
      tls: {
        rejectUnauthorized: false
      } 
    });

    let info = await transporter.sendMail({
      from: '"Child Hub - Administrador" <child.hub.api2021@gmail.com>',
      to: email,
      subject: "Usted ha solicitado el recupero de contraseña",
      text: "Recupero de Contraseña",
      html: `<b>Por favor haga click en el siguiente link ${linkRecuperador} para recuperar la contraseña</b>`,
    });
  }
};