const Sequelize     = require('sequelize');
const Op            = Sequelize.Op;
const hijo = require('../models').hijo;
const usuario= require('../models').usuario;
const grupo_sanguineo = require('../models').grupo_sanguineo;
module.exports = {
     async create(req, res,next) {
          // Usuario

          try {
               //Find the old User Object by the Id
               var usu =await usuario.findOne({
                    where: {
                         id: req.body.usuario,
                     }
               });

          } catch (e) {
               //return res.status(400).json( {message: e.message}) //{status: 400,
     
               return res.status(400).json({status:'error', error:e.message})
           }

          //  try {
          //      //Find the old User Object by the Id
          //      var grupo =await grupoSanguineo.findOne({
          //           where: {
          //                id: req.body.grupoSanguineo,
          //               }
          //      });

          // } catch (e) {
          //      //return res.status(400).json( {message: e.message}) //{status: 400,
     
          //      return res.status(400).json({status:'error', error:'No se encontraron hijos'})
          //  }
           
           var newHijo = new hijo({   
               idUsuario: usu.id,
               idGrupoSanguineo: req.body.grupoSanguineo,
               nombre: req.body.nombre,
               apellido: req.body.apellido,
               fecNacimiento:req.body.fechaNac,
               alergia:req.body.alergias,
               enfermedad:req.body.enfermedades,
               idGenero:req.body.genero
            })
         
            try {
               // Saving the User 
               var savedHijo = await newHijo.save();
               
               return res.status(200).json({hijo:savedHijo});
   
           } catch (e) {
               // return a Error message describing the reason 
               console.log(e)    
               return res.status(400).json({status: 400, message:e.message})
           }
     
           
     },
  list(_, res) {
     return hijo.findAll({
           include: [{
                model: usuario,
                as: 'usuario'
           },{
                model: grupoSanguineo,
                as: 'grupoSanguineo'
           }]
     })
     .then(hijo => res.status(200).send(hijo))
     .catch(error => res.status(400).send(error))
  },

  hijosPorPadre (req, res) {
       console.log(req);
    return hijo.findAll({
         where: {
          idUsuario: req.body.idUsuario,
         }
    })
    .then(hijo => res.status(200).send(hijo))
    .catch(error => res.status(400).send(error))
 },
 findOne (req, res) {
     return hijo.findOne({
         where: {
             id: req.body.id,
         }
     })
     .then(hijo => res.status(200).send(hijo))
     .catch(error => res.status(400).send(error))
  },

 async update (req, res) {

     try {
         //Find the old User Object by the Id
         var oldhijo =await hijo.findOne({
             where: {
                 id: req.body.id,
             },
         });
     } catch (e) {
         return res.status(400).json({status: 400, message: e.message})
     }
     // If no old User Object exists return false
     if (!oldhijo) {
         return false;
     }
     //Edit the User Object
     oldhijo.nombre = req.body.nombre
     oldhijo.apellido=req.body.apellido
     oldhijo.fecNacimiento = req.body.fecNacimiento
     oldhijo.alergia=req.body.alergia
     oldhijo.enfermedad=req.body.enfermedad
     oldhijo.idGrupoSanguineo=req.body.idGrupoSanguineo
     oldhijo.idGenero=req.body.idGenero

     try {
         var savedhijo = await oldhijo.save()
         return res.status(200).json({status: 200, data: savedhijo, message: "Los datos han sido actualizados correctamente"})
     } catch (e) {
         return res.status(400).json({status: 400, message: e.message})
     }
   
   }
  
};