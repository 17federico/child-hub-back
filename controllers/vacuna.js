const Sequelize     = require('sequelize');
const vacuna       = require('../models').vacuna;

module.exports = {
 create(req, res) {
    return vacuna
        .create ({
            descripcion:req.body.vacuna
             
        })
        .then(vacuna => res.status(200).send(vacuna))
        .catch(error => res.status(400).send(error))
 },
 list(_, res) {
     return vacuna.findAll({})
        .then(vacuna => res.status(200).send(vacuna))
        .catch(error => res.status(400).send(error))
 },
 find (req, res) {
     return vacuna.findAll({
         where: {
             id: req.body.id,
         }
     })
     .then(vacuna => res.status(200).send(vacuna))
     .catch(error => res.status(400).send(error))
  },
};