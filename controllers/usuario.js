const Sequelize     = require('sequelize');
const usuario       = require('../models').usuario;
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env]
const mailSender = require('./mailSender');


module.exports = {
    
 async create(req, res) {

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);

         var newUsuario = new usuario({   
            dni: req.body.dni,
            mail: req.body.mail,
            nombre: req.body.nombre,
            apellido:req.body.apellido,
            password:hashedPassword,
            telefono:req.body.telefono
         })
             
        try {

            var _details = await usuario.findOne({
                where: {
                    mail: req.body.mail,
                },
            });
            
            console.log("login:", _details)
    
            if (_details)  return res.status(202).json({status: 202, message: "Email ya existente, recupere su contraseña para ingresar con este mail"})

            var _details = await usuario.findOne({
                where: {
                    dni: req.body.dni,
                },
            });

            console.log("login:", _details)
    
            if (_details)  return res.status(202).json({status: 202, message: "DNI ya existente"})

            // Saving the User 
            var savedUser = await newUsuario.save();
            var token = jwt.sign({
                id: savedUser._id
            },config.SECRET, {
                expiresIn: 86400 // expires in 24 hours
            });

            return res.status(201).json({token:token, usuario:savedUser});

        } catch (e) {
            // return a Error message describing the reason 
            console.log(e)    
            return res.status(400).json({status: 400, message: e.message})
        }

 },
 list(_, res) {
     return usuario.findAll({})
        .then(usuario => res.status(200).send(usuario))
        .catch(error => res.status(400).send(error))
 },
 find (req, res) {
     return usuario.findAll({
         where: {
             dni: req.body.dni,
         }
     })
     .then(usuario => res.status(200).send(usuario))
     .catch(error => res.status(400).send(error))
  },

  findOne (req, res) {
    return usuario.findOne({
        where: {
            id: req.body.id,
        }
    })
    
    .then(usuario => res.status(200).send(usuario))
    .catch(error => res.status(400).send(error))
 },

async loginUser (req,res) {
    // console.log("req", req);
    // Creating a new Mongoose Object by using the new keyword
    try {
        // Find the User 
        

        var _details = await usuario.findOne({
            where: {
                mail: req.body.email,
            },
        });
        
        console.log("login:", _details)

        if (!_details)  return res.status(202).json({status: 202, message: "Invalid username"})
        
        var passwordIsValid = bcrypt.compareSync(req.body.password, _details.password);

        if (!passwordIsValid)  return res.status(203).json({status: 203, message: "Invalid username or password"})


        var token = jwt.sign({
            id: _details._id
        },config.SECRET, {
            expiresIn: 86400 // expires in 24 hours
        });

        return res.status(201).json({token:token, usuario:_details})

    } catch (e) {
        // return a Error message describing the reason     
        return res.status(400).json({status: 400, message: e.message})
    }

  },

  async update (req, res) {

    try {
        //Find the old User Object by the Id
        var oldUser =await usuario.findOne({
            where: {
                id: req.body.id,
            },
        });
    } catch (e) {
        return res.status(400).json({status: 400, message: e.message})
    }
    // If no old User Object exists return false
    if (!oldUser) {
        return false;
    }
    //Edit the User Object
    oldUser.nombre = req.body.nombre
    oldUser.mail = req.body.mail
    oldUser.apellido=req.body.apellido
    oldUser.telefono=req.body.telefono
    oldUser.dni=req.body.dni

    try {
        var savedUser = await oldUser.save()
        return res.status(200).json({status: 200, data: savedUser, message: "Succesfully Updated User"})
    } catch (e) {
        return res.status(400).json({status: 400, message: e.message})
    }
  
  },

  async updatePassword (req, res,next) {

    try {
        //Find the old User Object by the Id
        var oldUser =await usuario.findOne({
            where: {
                mail: req.body.mail,
            },
        });
    } catch (e) {
        return res.status(400).json({status: 400, message: e.message})
    }
    // If no old User Object exists return false
    if (!oldUser) {
        return  res.status(400).json({status: 400, message:"Mail no valido o inexistente"});
    }

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);

        //Edit the User Object
        oldUser.password=hashedPassword;

    try {
        //var savedUser = 
        await oldUser.save()
        return res.status(200).json({message: "Contraseña actualizada correctamente."})
    } catch (e) {
        return res.status(400).json({ message: e.message})
    }
},

async recoveryPassword(req, res, next) {

    // Creating a new Mongoose Object by using the new keyword
    try {
        let _details = await usuario.findOne({
            where: {
                mail: req.body.mail,
            },
        });
        console.log(_details);
        let token = jwt.sign({
            id: _details._id
        }, config.SECRET, {
            expiresIn: 86400 // expires in 24 hours
        });
            mailSender.sendEmailToRecoveryPass(req.body.mail, token)
            console.log("Mail enviado");
        return res.status(200).json({ message: "Hemos enviado un link a tu correo para que puedas cambiar tu contraseña" })

    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message })
    }
}


};