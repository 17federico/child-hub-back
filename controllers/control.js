const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const hijo = require('../models').hijo;
const control = require('../models').control;

module.exports = {
    async create(req, res,next) {
        // Usuario
    //     try {
    //       //Find the old User Object by the Id
    //       var hijos =await hijo.findOne({
    //            where: {
    //                 id: req.body.hijo,
    //             }
    //       });

    //  } catch (e) {
    //       //return res.status(400).json( {message: e.message}) //{status: 400,

    //       return res.status(400).json({status:'error', error:e.message})
    //   }

      var newControl = new control({   
          idHijo: req.body.hijo,
          fecControl: req.body.fecControl,
          peso: req.body.peso,
          altura: req.body.altura,
          diametroCabeza: req.body.diametroCabeza,
          medicamento: req.body.medicamentos,
          ObsMedicamento: req.body.obsMedicamento,
          estudioMedico: req.body.estudioMedico,
          ObsEstudioMedico: req.body.obsEstudioMedico,
          observaciones: req.body.observaciones

       })
    
       try {
          // Saving the User 
          var savedControl = await newControl.save();
          
          return res.status(200).json({control:savedControl});

      } catch (e) {
          // return a Error message describing the reason 
          console.log(e)    
          return res.status(400).json({status: 400, message:e.message})
      }

   },

list(_, res) {
   return controlPediatrico.findAll({
      
   })
   .then(controlPediatrico => res.status(200).send(controlPediatrico))
   .catch(error => res.status(400).send(error))
},

    
    list(_, res) {
        return control.findAll({
            include: [{
                model: hijo,
                as: 'hijo'
            }]
        })
            .then(control => res.status(200).send(control))
            .catch(error => res.status(400).send(error))
    },

    find(req, res) {
        return control.findAll({
            where: {
                id: req.body.id,
            },
            include: [{
                model: hijo,
                as: 'hijo'
            }]
        })
            .then(control => res.status(200).send(control))
            .catch(error => res.status(400).send(error))
    },

    findPorHijo(req, res) {
        return control.findAll({
            where: {
                idHijo: req.body.idHijo,
            },
            order: [['fecControl', 'DESC']]
        })
            .then(control => res.status(200).send(control))
            .catch(error => res.status(400).send(error))
    },


    async deleteControl(req, res) {

        // Delete the User
        try {
            var deleted = await control.destroy({
                where: {
                    id: req.body.id,
                }
            })
            if (deleted.n === 0 && deleted.ok === 1) {
                return res.status(400).json({ status: 400, message: "El control no se puede borrar" })
            }
            return res.status(200).json({ status: 200, data: deleted, message: "El control ha sido borrado exitosamente" })

        } catch (e) {
            return res.status(400).json({ status: 400, message: e.message })
        }
    },

    async update(req, res) {

        try {
            //Find the old User Object by the Id
            var oldControl = await control.findOne({
                where: {
                    id: req.body.id,
                },
            });
        } catch (e) {
            return res.status(400).json({ status: 400, message: e.message })
        }
        // If no old User Object exists return false
        if (!oldControl) {
            return false;
        }
        //Edit the User Object
        oldControl.fecControl = req.body.fecControl,
            oldControl.peso = req.body.peso,
            oldControl.altura = req.body.altura,
            oldControl.diametroCabeza = req.body.diametroCabeza,
            oldControl.medicamento = req.body.medicamento,
            oldControl.ObsMedicamento = req.body.ObsMedicamento,
            oldControl.estudioMedico = req.body.estudioMedico,
            oldControl.ObsEstudioMedico = req.body.ObsEstudioMedico,
            oldControl.observaciones = req.body.observaciones

        try {
            var savedControl = await oldControl.save()
            return res.status(200).json({ status: 200, data: savedControl, message: "Los datos han sido actualizados correctamente" })
        } catch (e) {
            return res.status(400).json({ status: 400, message: e.message })
        }

    },

    findAll(req, res){
        return control.findAll({
            where:{
                idHijo: req.body.idHijo,
            },
            attributes: [
            Sequelize.fn('MAX', Sequelize.col('control.id')), 'peso', 'altura', 'diametroCabeza'
        ],
            include: [{
                model: hijo,
                as: 'hijo'
            }],
        raw: true
        })
        .then(control => res.status(200).send(control))
        .catch(error => res.status(400).send(error))
  }
};``