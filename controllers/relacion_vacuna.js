const Sequelize     = require('sequelize');
const Op            = Sequelize.Op;
const hijo = require('../models').hijo;
const relacion_vacuna= require('../models').relacion_vacuna;
const vacuna = require('../models').vacuna;

module.exports = {
  create(req, res) {
          // Busco Hijo
          const responseHijo = hijo.findOne({
               where: {
                   [Op.or]: [{
                        nombre: req.body.hijo
                   },{
                        id: req.body.hijo
                   }]
               }
          });
          // Busco Vacuna
          const responseVacuna = vacuna.findOne({
               where: {
                   [Op.or]: [{
                        descripcion: req.body.vacuna
                   },{
                        id: req.body.vacuna
                   }]
               }
          });
          Promise
          .all ([responseHijo, responseVacuna])
          .then(responses => {
               return relacion_vacuna
                   .create ({
                        idHijo: responses[0].id,
                        idVacuna: responses[1].id,
                        fecAplicacion:req.body.fecAplicacion,
                        lugarAplicacion:req.body.lugarAplicacion,
                        dosis:req.body.dosis
                   })
                   .then(relacion_vacuna => res.status(200).send(relacion_vacuna))
           })
           .catch(error => res.status(400).send(error));
     },
  list(_, res) {
     return relacion_vacuna.findAll({
           include: [{
                model: vacuna,
                as: 'vacuna'
           },{
                model: hijo,
                as: 'hijo'
           }]
     })
     .then(relacion_vacuna => res.status(200).send(relacion_vacuna))
     .catch(error => res.status(400).send(error))
  },

  find (req, res) {
    return relacion_vacuna.findAll({
         where: {
               id: req.body.id,
         },
         include: [{
               model: vacuna,
               as: 'vacuna'
          },{
               model: hijo,
               as: 'hijo'
          }]
    })
    .then(relacion_vacuna => res.status(200).send(relacion_vacuna))
    .catch(error => res.status(400).send(error))
 },

 findPorHijo (req, res) {
    return relacion_vacuna.findAll({
         where: {
               idHijo: req.body.idHijo,
         },
         include: [{
               model: vacuna,
               as: 'vacuna'
          },{
               model: hijo,
               as: 'hijo'
          }]
    })
    .then(relacion_vacuna => res.status(200).send(relacion_vacuna))
    .catch(error => res.status(400).send(error))
 },

 async deleteVacunacion (req,res) {

     // Delete the User
     try {
         var deleted = await relacion_vacuna.destroy({
             where: {
                 id: req.body.id,
             }
         })
         if (deleted.n === 0 && deleted.ok === 1) {
             return res.status(400).json({status: 400, message: "La vacunación no se puede borrar"})
         }
         return res.status(200).json({status: 200, data: deleted, message: "La vacuna aplicada ha sido borrado exitosamente"})
 
     } catch (e) {
         return res.status(400).json({status: 400, message: e.message})
     }
 },

 async update (req, res) {

     try {
         //Find the old User Object by the Id
         var oldvacuna =await relacion_vacuna.findOne({
             where: {
                 id: req.body.id,
             },
         });
     } catch (e) {
         return res.status(400).json({status: 400, message: e.message})
     }
     // If no old User Object exists return false
     if (!oldvacuna) {
         return false;
     }
     //Edit the User Object
     oldvacuna.lugarAplicacion = req.body.lugarAplicacion
     oldvacuna.fecAplicacion = req.body.fecAplicacion
     oldvacuna.idvacuna=req.body.idvacuna
     oldvacuna.dosis=req.body.dosis

     try {
         var savedvacuna = await oldvacuna.save()
         return res.status(200).json({status: 200, data: savedvacuna, message: "Los datos han sido actualizados correctamente"})
     } catch (e) {
         return res.status(400).json({status: 400, message: e.message})
     }
   
   }
  
};